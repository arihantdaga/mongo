# Overview
This plugin provides the ability for your VolantMQ server to perform authentication (determining who can log in) and authorisation (determining what pub/sub permissions they have) using Mongodb as backend. 

## Configuring and enabling the Plugin

```yaml
plugins:
  enabled:
    - auth_mongo
  config:
    auth: # plugin type
      - name: authmongo
        backend: mongo
        config:
          mongoURI: mongodb://localhost:27017/mqtt
          database: mqtt
          collection: mqtt_user          
```

## Example doc in your mongodb collection

```JSON
{
  "username": "user1",
  "password":"pass1",
  "subscribe": ["/topic/1", "/topic/2", "topic/+/3", "topic/4/#"], // Wild cards supported
  "publish": ["/topic/1", "topic/2"]
}

```

