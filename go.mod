module gitlab.com/VolantMQ/vlplugin/auth/mongo

go 1.14

require (
	github.com/VolantMQ/vlapi v0.5.4
	github.com/go-bongo/bongo v0.10.4
	github.com/maxwellhealth/go-dotaccess v0.0.0-20190924013105-74ea4f4ca4eb // indirect
	github.com/oleiade/reflections v1.0.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v3 v3.0.0-20200121175148-a6ecf24a6d71
)
